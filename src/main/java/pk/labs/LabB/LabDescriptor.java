package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.display.wyswietlacz";
    public static String controlPanelImplClassName = "pk.labs.LabB.controlpanel.panelKontrolny";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.IMycie";
    public static String mainComponentImplClassName = "pk.labs.LabB.main.Mycie";
    public static String mainComponentBeanName = "mycie";
    // endregion

    // region P2
    public static String mainComponentMethodName = "...";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "...";
    // endregion
}
